# superBlendR

Dynamic xray provides physiological and potentially functional
information at lower dose than high-res x-ray.  This package seeks to apply
super resolution and related techniques to blend information from multiple
spatial and temporal scales to improve image resolution.

### installation

```
devtools::install_bitbucket( "stnava/superblendr")
```

or if you have ANTsR etc installed then clone the repo and do:

```
cd superblendr
R CMD INSTALL .
```

### getting started

in `R`, below is effectively a test of basic functionality.


```
library( ANTsR )
library( ANTsRNet )
library( keras )
library( superBlendR )
nframes = 10
domain = makeImage(  c( 256, 256, nframes ) )
dynImageL = list()
trnImageL = list()
siminds = sample( 1:6, nframes,  replace = T )
for ( k in 1:nframes ) {
  temp = ri( siminds[k] )
  dynImageL[[k]] = temp
  }
simindsshift = siminds[ (((1:nframes)+2) %% nframes)+1 ]
for ( k in 1:(nframes-2) ) {
  temp = ri( simindsshift[k] ) # simulate a different ordering/length for transmission image
  trnImageL[[k]] = exp( temp / 100 )
  }
dynImage = mergeListToNDImage( domain, dynImageL )
trnImage = mergeListToNDImage( domain, trnImageL )
frame = selectReferenceFrame( dynImage )
staticImage = getReferenceFrame( dynImage, frame )
reg = registerDynamicToStaticFrame( dynImage, frameIndex = frame ) # try both options
reg2 = registerDynamicToStaticFrame( dynImage, frame = staticImage, frameIndex = frame  )
srDynamicAvg = antsAverageImages( reg$alignedImages )
srDynamicAvg = linMatchIntensity( srDynamicAvg, staticImage )
match = matchTransmissionToLogImage( trnImage, staticImage, frame )
# if you have a model, you can run this below
# sr = applySuperResolutionModel( staticImage, mdl, c( -127.5, 127.5 ) )
blended = superBlendAlpha( match$transWarped3D, srDynamicAvg, staticImage  )
```
